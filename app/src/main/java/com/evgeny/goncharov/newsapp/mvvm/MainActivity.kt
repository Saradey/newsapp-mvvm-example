package com.evgeny.goncharov.newsapp.mvvm

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.evgeny.goncharov.newsapp.mvvm.di.ActivitySubComponent
import com.evgeny.goncharov.newsapp.mvvm.feature.common.screen.SplashScreenFragment
import com.evgeny.goncharov.newsapp.mvvm.managers.route.IRouter
import com.evgeny.goncharov.newsapp.mvvm.managers.route.RouterImpl
import javax.inject.Inject

class MainActivity : AppCompatActivity() {


    companion object {
        lateinit var activityComponent: ActivitySubComponent
    }

    @Inject
    lateinit var factory: ActivitySubComponent.Factory



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.appComponent.inject(this)

        activityComponent = factory.create(this)

        supportFragmentManager.beginTransaction()
            .add(R.id.frmField, SplashScreenFragment.getInstance())
            .commit()
    }


}
