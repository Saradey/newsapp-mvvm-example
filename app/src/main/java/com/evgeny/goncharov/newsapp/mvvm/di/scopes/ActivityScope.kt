package com.evgeny.goncharov.newsapp.mvvm.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope {
}