package com.evgeny.goncharov.newsapp.mvvm.managers.date

import com.evgeny.goncharov.newsapp.mvvm.common.FORMAT_YEAR_MONTH_DATE
import java.text.SimpleDateFormat
import java.util.*

class DateFormatter :
    IDateFormatter {

    private val locale = Locale(Locale.getDefault().language)

    private val sdfYearMonthDate = SimpleDateFormat(FORMAT_YEAR_MONTH_DATE, locale)


    override fun getSdfYearMonthDate(): SimpleDateFormat {
        return sdfYearMonthDate
    }

}