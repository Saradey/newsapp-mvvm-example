package com.evgeny.goncharov.newsapp.mvvm.feature.common.screen

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.evgeny.goncharov.newsapp.mvvm.MainActivity
import com.evgeny.goncharov.newsapp.mvvm.R
import com.evgeny.goncharov.newsapp.mvvm.base.BaseFragment
import com.evgeny.goncharov.newsapp.mvvm.managers.route.IRouter
import kotlinx.android.synthetic.main.fragment_splash_screen.*
import javax.inject.Inject

class SplashScreenFragment : BaseFragment() {

    companion object {
        fun getInstance(): SplashScreenFragment {
            val fragment = SplashScreenFragment()
            return fragment
        }
    }

    @Inject
    lateinit var router: IRouter


    override fun getLayoutId(): Int = R.layout.fragment_splash_screen


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.activityComponent.inject(this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        startAnimationLogo()
    }


    private fun startAnimationLogo() {
        val animator = AnimationUtils.loadAnimation(context, R.anim.show_logo_anim)
        imvLogoScreen.startAnimation(animator)
        animator.setAnimationListener(createAnimationListener(::endAnimationLogo))
    }


    private fun endAnimationLogo() {
        val animator = AnimationUtils.loadAnimation(context, R.anim.hide_logo_anim)
        imvLogoScreen.startAnimation(animator)
        animator.setAnimationListener(createAnimationListener(::goToTheNextFragment))
    }


    private fun createAnimationListener(actionEnd: () -> Unit): Animation.AnimationListener {
        return object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                actionEnd()
            }

            override fun onAnimationStart(p0: Animation?) {
            }
        }
    }


    private fun goToTheNextFragment() {
        router.startApplication()
    }
}