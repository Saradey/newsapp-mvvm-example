package com.evgeny.goncharov.newsapp.mvvm.di.module

import android.content.Context
import com.evgeny.goncharov.newsapp.mvvm.App
import com.evgeny.goncharov.newsapp.mvvm.di.scopes.ApplicationScope
import com.evgeny.goncharov.newsapp.mvvm.managers.date.DateFormatter
import com.evgeny.goncharov.newsapp.mvvm.managers.date.IDateFormatter
import com.evgeny.goncharov.newsapp.mvvm.repository.AuthorizationRepositoryImpl
import com.evgeny.goncharov.newsapp.mvvm.repository.IAuthorizationRepository
import dagger.Binds
import dagger.Module
import javax.inject.Named


@Module
interface AppBindsModule {

    @Binds
    @Named("Application")
    fun bindAppContext(app: App): Context


    @Binds
    @ApplicationScope
    fun bindDateFormatter(formatter: DateFormatter): IDateFormatter


    @Binds
    @ApplicationScope
    fun bindAuthorizationRepository(AuthorizationRepository: AuthorizationRepositoryImpl)
            : IAuthorizationRepository


}