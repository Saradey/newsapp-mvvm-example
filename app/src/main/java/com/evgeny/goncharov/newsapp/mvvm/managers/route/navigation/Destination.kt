package com.evgeny.goncharov.newsapp.mvvm.managers.route.navigation

sealed class Destination {

    object AuthScreen : Destination()

    object RegScreen : Destination()

    object SplashScreen : Destination()

    object SettingsScreen : Destination()

    object MainNewsScreen : Destination()

    object WallNewsScreen : Destination()

    object DescriptionNewsScreen : Destination()

    object ProfileScreen : Destination()

    object EditProfileScreen : Destination()

    object Back : Destination()
}