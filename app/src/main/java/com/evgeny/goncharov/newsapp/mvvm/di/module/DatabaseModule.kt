package com.evgeny.goncharov.newsapp.mvvm.di.module

import android.content.Context
import androidx.room.Room
import com.evgeny.goncharov.newsapp.mvvm.database.AppDatabase
import com.evgeny.goncharov.newsapp.mvvm.database.dao.UserDao
import com.evgeny.goncharov.newsapp.mvvm.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class DatabaseModule {

    @Provides
    @ApplicationScope
    fun provideAppDataBase(@Named("Application") context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "piska").build()
    }

    @Provides
    fun provideUserDao(appDataBase: AppDatabase): UserDao {
        return appDataBase.createUserDao()
    }

}