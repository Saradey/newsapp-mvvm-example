package com.evgeny.goncharov.newsapp.mvvm.managers.route

import com.evgeny.goncharov.newsapp.mvvm.MainActivity
import com.evgeny.goncharov.newsapp.mvvm.repository.IAuthorizationRepository
import javax.inject.Inject

class RouterImpl @Inject constructor(
    private val activity: MainActivity,
    private val authorizationRepository: IAuthorizationRepository
) : IRouter {


    override fun startApplication() {
        if (authorizationRepository.isAutherUser()) {

        } else {

        }
    }


}