package com.evgeny.goncharov.newsapp.mvvm.repository

import android.content.Context
import com.evgeny.goncharov.newsapp.mvvm.common.AUTHORIZATION_BOOLEAN
import com.evgeny.goncharov.newsapp.mvvm.common.AUTHORIZATION_PREFERENCES
import com.evgeny.goncharov.newsapp.mvvm.database.dao.UserDao
import javax.inject.Inject
import javax.inject.Named

class AuthorizationRepositoryImpl @Inject constructor(
    val userDao: UserDao,
    @Named("Application") val context: Context
) : IAuthorizationRepository {


    override fun isAutherUser(): Boolean {
        val autherShared =
            context.getSharedPreferences(AUTHORIZATION_PREFERENCES, Context.MODE_PRIVATE)
        return autherShared.getBoolean(AUTHORIZATION_BOOLEAN, false)
    }


}