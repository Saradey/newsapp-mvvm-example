package com.evgeny.goncharov.newsapp.mvvm.di

import com.evgeny.goncharov.newsapp.mvvm.MainActivity
import com.evgeny.goncharov.newsapp.mvvm.di.module.ActivityBindsModule
import com.evgeny.goncharov.newsapp.mvvm.di.scopes.ActivityScope
import com.evgeny.goncharov.newsapp.mvvm.feature.common.screen.SplashScreenFragment
import dagger.BindsInstance
import dagger.Subcomponent


@ActivityScope
@Subcomponent(modules = [ActivityBindsModule::class])
interface ActivitySubComponent {

    //fragments
    fun inject(fragment: SplashScreenFragment)

    fun inject(mainActivity: MainActivity)



    @Subcomponent.Factory
    interface Factory {

        fun create(
            @BindsInstance mainActivity: MainActivity
        ): ActivitySubComponent

    }

}