package com.evgeny.goncharov.newsapp.mvvm.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.evgeny.goncharov.newsapp.mvvm.common.VERSION_DATA_BASE
import com.evgeny.goncharov.newsapp.mvvm.database.converters.ConverterDate
import com.evgeny.goncharov.newsapp.mvvm.database.dao.UserDao
import com.evgeny.goncharov.newsapp.mvvm.feature.authorization.model.User

@Database(
    version = VERSION_DATA_BASE,
    entities = [
        User::class
    ]
)
@TypeConverters(*[ConverterDate::class])
abstract class AppDatabase : RoomDatabase() {
    abstract fun createUserDao(): UserDao
}