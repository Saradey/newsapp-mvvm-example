package com.evgeny.goncharov.newsapp.mvvm.feature.authorization.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.evgeny.goncharov.newsapp.mvvm.database.converters.ConverterDate
import java.util.*

@Entity(tableName = "user")
data class User(
    @PrimaryKey val id: Int = 0,
    var name: String? = null,
    var LastName: String? = null,
    @TypeConverters(*[ConverterDate::class]) val dateBirthDay: Date? = null,
    val age: Int? = null
)