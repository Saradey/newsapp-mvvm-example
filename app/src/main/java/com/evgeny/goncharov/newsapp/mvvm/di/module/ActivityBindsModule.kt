package com.evgeny.goncharov.newsapp.mvvm.di.module

import android.content.Context
import com.evgeny.goncharov.newsapp.mvvm.MainActivity
import com.evgeny.goncharov.newsapp.mvvm.di.scopes.ActivityScope
import com.evgeny.goncharov.newsapp.mvvm.managers.route.IRouter
import com.evgeny.goncharov.newsapp.mvvm.managers.route.RouterImpl
import dagger.Binds
import dagger.Module
import javax.inject.Named

@Module
interface ActivityBindsModule {

    @Binds
    @Named("MainActivity")
    fun bindActivityContext(mainActivity: MainActivity): Context

    @Binds
    @ActivityScope
    fun bindRouter(router: RouterImpl): IRouter

}