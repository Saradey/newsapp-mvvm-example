package com.evgeny.goncharov.newsapp.mvvm.di.module

import com.evgeny.goncharov.newsapp.mvvm.di.ActivitySubComponent
import dagger.Module

@Module(subcomponents = [ActivitySubComponent::class])
interface AppCreateSubComponentModule