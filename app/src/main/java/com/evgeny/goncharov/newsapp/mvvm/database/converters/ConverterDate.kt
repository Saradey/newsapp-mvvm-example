package com.evgeny.goncharov.newsapp.mvvm.database.converters

import androidx.room.TypeConverter
import com.evgeny.goncharov.newsapp.mvvm.App
import com.evgeny.goncharov.newsapp.mvvm.managers.date.IDateFormatter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class ConverterDate {

    @Inject
    lateinit var dateFormatter: IDateFormatter
    lateinit var format: SimpleDateFormat


    init {
        App.appComponent.inject(this)
        format = dateFormatter.getSdfYearMonthDate()
    }


    @TypeConverter
    fun dateToString(date: Date): String? {
        return format.format(date)
    }

    @TypeConverter
    fun stringToDate(date: String): Date? {
        return format.parse(date)
    }

}