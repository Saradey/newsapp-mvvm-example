package com.evgeny.goncharov.newsapp.mvvm.repository

interface IAuthorizationRepository {
    fun isAutherUser(): Boolean
}