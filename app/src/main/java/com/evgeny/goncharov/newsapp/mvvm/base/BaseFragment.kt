package com.evgeny.goncharov.newsapp.mvvm.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment : Fragment() {

    protected val disposables: CompositeDisposable = CompositeDisposable()

    abstract fun getLayoutId(): Int

    protected fun dispose() {
        disposables.clear()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = LayoutInflater.from(container?.context).inflate(
            getLayoutId(),
            container,
            false
        )
        return v
    }


    override fun onDestroy() {
        dispose()
        super.onDestroy()
    }
}