package com.evgeny.goncharov.newsapp.mvvm.managers.route

interface IRouter {

    fun startApplication()

}