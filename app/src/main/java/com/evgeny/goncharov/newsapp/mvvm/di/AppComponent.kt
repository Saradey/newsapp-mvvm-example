package com.evgeny.goncharov.newsapp.mvvm.di

import com.evgeny.goncharov.newsapp.mvvm.App
import com.evgeny.goncharov.newsapp.mvvm.MainActivity
import com.evgeny.goncharov.newsapp.mvvm.database.converters.ConverterDate
import com.evgeny.goncharov.newsapp.mvvm.di.module.AppBindsModule
import com.evgeny.goncharov.newsapp.mvvm.di.module.AppCreateSubComponentModule
import com.evgeny.goncharov.newsapp.mvvm.di.module.DatabaseModule
import com.evgeny.goncharov.newsapp.mvvm.di.scopes.ApplicationScope
import com.evgeny.goncharov.newsapp.mvvm.managers.date.DateFormatter
import dagger.BindsInstance
import dagger.Component


@ApplicationScope
@Component(
    modules = [
        AppBindsModule::class,
        DatabaseModule::class,
        AppCreateSubComponentModule::class
    ]
)
interface AppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(converter: ConverterDate)
    

    @Component.Factory
    interface Factory {

        fun create(
            @BindsInstance app: App,
            @BindsInstance formatter: DateFormatter
        ): AppComponent
    }
}