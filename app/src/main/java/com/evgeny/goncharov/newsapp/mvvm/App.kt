package com.evgeny.goncharov.newsapp.mvvm

import android.app.Application
import com.evgeny.goncharov.newsapp.mvvm.di.AppComponent
import com.evgeny.goncharov.newsapp.mvvm.di.DaggerAppComponent
import com.evgeny.goncharov.newsapp.mvvm.managers.date.DateFormatter


class App : Application() {


    companion object {
        lateinit var appComponent: AppComponent
    }


    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.factory()
            .create(
                this,
                DateFormatter()
            )
    }


}