package com.evgeny.goncharov.newsapp.mvvm.managers.date

import java.text.SimpleDateFormat

interface IDateFormatter {
    fun getSdfYearMonthDate(): SimpleDateFormat
}